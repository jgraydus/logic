{-# LANGUAGE OverloadedStrings #-}
module Logic where

import Data.String
import Control.Monad.State
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (maybeToList, listToMaybe)

-- let's start by defining a little DSL (domain specific language) to
-- make entering formulas more convenient.

data Prop = -- short for Proposition
  Var String    | 
  Not Prop      | 
  Prop :-> Prop | 
  Prop :| Prop  | 
  Prop :& Prop
  deriving (Eq, Ord)

infix 3 :->
infixl 4 :|
infixl 5 :&

instance Show Prop where
  show (Var s) = s
  show (Not p) = "¬" ++ (show p)
  show (p1 :-> p2) = "(" ++ (show p1) ++ " → " ++ (show p2) ++ ")"
  show (p1 :| p2) = "(" ++ (show p1) ++ " ∨ " ++ (show p2) ++ ")"
  show (p1 :& p2) = "(" ++ (show p1) ++ " ∧ " ++ (show p2) ++ ")"

-- GHC hack. we can do "A" :& "B" instead of Var "A" :& Var "B", etc.
instance IsString Prop where
  fromString = Var

-- our Prop data type is nice for generating formulas, but it's a 
-- horrible format for processing. what we would like is conjunctive
-- normal form.  cnf removes implications and moves all conjunctions
-- to the outer level.  we can then make the conjunctions implicit 
-- instead of explicit by transforming into a list of clauses where a
-- clause is a Prop that consists of only variables, negated variables,
-- and disjunctions

cnf :: Prop -> [Prop]
cnf p@(Var _) = [p]
cnf p@(Not (Var _)) = [p]
cnf (Not (Not p)) = cnf p
cnf (p :-> q) = cnf $ Not p :| q
cnf (Not (p :-> q)) = cnf $ p :& Not q
cnf (Not (p :| q)) = cnf $ Not p :& Not q     -- DeMorgan's laws
cnf (Not (p :& q)) = cnf $ Not p :| Not q
cnf (p :& q) = (cnf p) ++ (cnf q)
cnf (p :| q) = [r :| s | r <- cnf p, s <- cnf q] -- generalized distribution

-- now each clause is an explicit list of disjunctions, so we can
-- repeat the previous process and turn them into lists of implicit disjunctions

dnf :: Prop -> [Prop]
dnf (p :| q) = (dnf p) ++ (dnf q)
dnf p@(Var _) = [p]
dnf p@(Not (Var _)) = [p]

-- for readability
type Clause = Set Prop

-- transform raw Prop input into internal representation Set Clause
simplify :: Prop -> Set Clause
simplify = Set.fromList . map (Set.fromList . dnf) . cnf

-- we can determine whether a proposition constitutes a tautology
-- by negating the proposition and repeatedly applying a resolution
-- rule until we find a contradiction (or not)
resolve :: Prop -> Bool
resolve = (flip evalState Set.empty) . res . simplify . Not

neg p@(Var _) = Not p
neg (Not p@(Var _)) = p

-- apply resolution rule until we have a result
res :: Set Clause -> State (Set Clause) Bool
res cs =  case Set.toList cs of
    -- if there are no clauses left, then we have failed to find a
    -- contradiction so the original formula was not a tautology
    [] -> return False
    -- if an application of the resolution rule produced an empty
    -- clause, then there must have been a contradiction
    xs | any Set.null xs -> return True
    -- if neither of the base cases has occurred, continue by applying
    -- the resolution rule for the next Clause to other remaining
    -- clauses and recursing
    (x:xs) -> do
      modify (Set.insert x)
      seen <- get
      let rs = resolvents x xs
      res (Set.difference (Set.fromList (xs ++ rs)) seen)

resolvents :: Clause -> [Clause] -> [Clause]
resolvents c cs = do
  x <- cs
  maybeToList $ resolvent c x

-- if literal l is an element of c1 and ~l is an element of c2, then
-- their resolvent of c1 and c2 is the union of c1 and c2 minus l and ~l
resolvent :: Clause -> Clause -> Maybe Clause
resolvent c1 c2 = do
  x <- listToMaybe $ Set.toList $ Set.intersection c1 (Set.map neg c2)
  return $ (Set.filter (/= x) . Set.filter (/= (neg x))) (Set.union c1 c2)

{-
If my client is guilty, then the knife was in the drawer. Either the
knife was not in the drawer or Jason pritchard saw the knife. If the
knife was not there on October 10, it follows that Jason Pritchard
did not see the knife. Furthermore, if the knife was there on
October 10, then the knife was in the drawer and also the hammer was
in the barn. But we all know that the hammer was not in the barn
Therefore, ladies and gentlemen of the jury, my client is innocent.

A = my client is guilty
B = the knife was in the drawer
C = Jason Pritchard saw the knife
D = the knife was there on October 10
E = the hammer was in the barn
-}

p = ("A" :-> "B") :& (Not "B" :| "C") :& (Not "D" :-> Not "C") :& ("D" :-> ("B" :& "E")) :& (Not "E") :-> Not "A"
