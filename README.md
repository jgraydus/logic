This is a simple toy project for determining the truth of statements in propositional logic.

Example usage:

When the sky is dark on a summer day, then it is going to rain.  It didn't rain today.  Therefore, either today was not a summer day, or the sky was not dark.

    > -- A = The sky is dark
    > -- B = It is a summer day
    > -- C = It rained
    > ("A" :& "B" :-> "C") :& Not "C" :-> (Not "A" :| Not "B")
    ((((A ∧ B) → C) ∧ ¬C) → (¬A ∨ ¬B))
    > resolve $ ("A" :& "B" :-> "C") :& Not "C" :-> (Not "A" :| Not "B")
    True

The motivating example for this exercise (which is far more complicated) is in the source code.